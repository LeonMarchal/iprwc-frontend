import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './user/register/register.component';
import {LoginComponent} from './user/login/login.component';
import {ListComponent} from './user/list/list.component';
import {PlistComponent} from './product/list/plist.component';
import {AddproductComponent} from "./product/addproduct/addproduct.component";
import {ClistComponent} from "./cart/clist/clist.component";


export const routes: Routes =
  [
    {path: '', component: HomeComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'addProduct', component: AddproductComponent},
    {path: 'login', component: LoginComponent},
    {path: 'users', component: ListComponent},
    {path: 'products', component: PlistComponent},
    {path: 'cart', component: ClistComponent}


  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutesModule {
}
