import {Component} from '@angular/core';
import {ListDataSource} from './list.datasource';
import {UserService} from '../user.service';
import {AuthorizationService} from "../../shared/authorization.service";
import {User} from "../user";
import {Product} from "../../product/product";

@Component({
  selector: 'user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  public displayedColumns = ['fullName', 'postcode', 'streetnumber', 'emailAddress', 'roles'];
  public dataSource = null;
  users: User[];
  public Roles = [
    {value: 'ADMIN', viewValue: 'ADMIN'},
    {value: 'GUEST', viewValue: 'GUEST'}
  ];



  constructor(private userService: UserService, private authService: AuthorizationService) {
    let user: User = this.authService.getAuthenticator();
    if (user.roles == 'ADMIN') {
      this.getUsersList();
    } else alert("Geen toegang")
  }

  private getUsersList() {
    this.userService.getAll().subscribe(
      data => {
        this.users = data
        this.dataSource = new ListDataSource(data);
      }
    );
  }

  onSave(user: User) {
    this.userService.update(user);
  }

  public remove(user: User) {
    this.userService.remove(user);
    setTimeout( callback => {
      this.getUsersList();
    },1000);
  }

  public hasData() {
    return this.dataSource !== null;
  }


}
