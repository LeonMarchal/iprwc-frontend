import { NgModule } from '@angular/core';
import { ClistComponent } from './clist/clist.component';
import {PublicModule} from "../public.module";
import {CartService} from "./cart.service";

@NgModule({
  imports: [PublicModule],
  declarations: [ClistComponent],
  providers: [CartService]
})
export class CartModule { }
