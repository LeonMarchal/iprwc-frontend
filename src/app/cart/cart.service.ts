import { Injectable } from '@angular/core';
import {ApiService} from "../shared/api.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Cart} from "../cart/cart";
import {ClistComponent} from "./clist/clist.component";

@Injectable()
export class CartService {

  constructor(private api: ApiService) { }

  public getCarstByEmail(email: String): Observable<Cart[]> {
    return this.api.get<Cart[]>('cart/' + email);
  }

  public getCarstByEmailAndProduct(email: String,product: string): Observable<Cart[]> {
    return this.api.get<Cart[]>('cart/' + email);
  }


  addToCart(cart: Cart) {
    let data =
      {
        id: cart.id,
        email: cart.email,
        productId: cart.productId
      };

    this.api.post<void>('cart', data).subscribe
    (
      data => {
      },
      error => {
        alert('toevoegen is mislukt');
      }
    );
  }

  public removeFromCard(id: string) {
    this.api.delete('cart/' + id).subscribe();
  }

  public removeByMail() {
    this.api.delete("cart/mail").subscribe();
  }
}
