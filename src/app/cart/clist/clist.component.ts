import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ProductService} from "../../product/product.service";
import {AuthorizationService} from "../../shared/authorization.service";
import {User} from "../../user/user";
import {CartService} from "../cart.service";
import {Cart} from "../cart";
import {ClistDatasource} from "./clist.datasource";
import {Product} from "../../product/product";

@Component({
  selector: 'app-clist',
  templateUrl: './clist.component.html',
  styleUrls: ['./clist.component.css']
})
export class ClistComponent implements OnInit {
  // public displayedColumns = ['productName', 'discription', 'price', 'pictureLocation', 'btn'];
  public displayedColumns = ['productName', 'price', 'btn'];

  public dataSource = null;
  public cartItems: Cart[];
  public productsList: Product[] = [];
  operationsCompleted: number = 0;
  public totalPrice:number = 0;


  constructor(private productService: ProductService, private cartService: CartService, private authService: AuthorizationService) {
    let user: User = this.authService.getAuthenticator();
    this.getCartByMail(user.emailAddress);

  }


  public getCartByMail(email: String) {
    this.cartService.getCarstByEmail(email).subscribe(
      carts => {
        this.cartItems = carts;
        this.getProducts(this.cartItems);
      }
    );
  }

  private getProducts(carts: Cart[]) {
    for (var i = 0, len = carts.length; i < len; i++) {
      this.productService.getById(carts[i].productId).subscribe(
        product => {
          this.productsList.push(product[0]);
          this.operation()
          this.totalPrice = this.round(this.totalPrice,2) + this.round(Number(product[0].price),2);
        });

    };
  }

  private operation() {
    this.operationsCompleted++;
    if (this.operationsCompleted == this.cartItems.length) this.afterLoop();
  }

  private afterLoop() {
    this.dataSource = new ClistDatasource(this.productsList);
  }

  public hasData() {
    return this.dataSource !== null;
  }

  public removeFromCart(id:string) {
    this.cartService.removeFromCard(id);
    setTimeout( callback => {
    this.dataSource = null;
    this.cartItems = [];
    this.productsList = [];
    this.operationsCompleted = 0;
    this.totalPrice = 0;
    let user: User = this.authService.getAuthenticator();
      this.getCartByMail(user.emailAddress);
    },500);
  }

  public round(number, precision) {
    var factor = Math.pow(10, precision);
    var tempNumber = number * factor;
    var roundedTempNumber = Math.round(tempNumber);
    return roundedTempNumber / factor;
  }

  public proceedOrder(){
  this.cartService.removeByMail();
  setTimeout( callback => {
  this.dataSource = null;
  this.cartItems = [];
  this.productsList = [];
  this.operationsCompleted = 0;
  this.totalPrice = 0;
  let user: User = this.authService.getAuthenticator();
  this.getCartByMail(user.emailAddress);
},500);
alert("Order is bevestigd")
}

  ngOnInit() {
  }
}
