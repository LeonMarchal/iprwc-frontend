import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Product} from "../../product/product";
import {DataSource} from "@angular/cdk/collections";


export class ClistDatasource extends DataSource<any> {
  constructor(private products: Product[]) {
    super();
  }

  public connect(): Observable<Product[]> {
    return Observable.of(this.products);
  }

  public disconnect() {
  }

}
