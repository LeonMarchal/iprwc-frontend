import {Component} from '@angular/core';

import {AuthorizationService} from '../shared/authorization.service';

import {User} from '../user/user';
import {ProductService} from "../product/product.service";
import {Product} from "../product/product";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  public authenticated: boolean = false;
  public logo = ''
  public userName = '';
  public products:Product[] = [];
  public admin: boolean = false;

  constructor(private authService: AuthorizationService, private productService: ProductService, private router: Router) {
    authService.authorized$.subscribe(
      authorized => {
        this.updateAuthentication();
      }
    );

    this.updateAuthentication();
    this.getSampleProducts();
  }

  private updateAuthentication() {
    this.authenticated = this.authService.hasAuthorization();

    if (!this.authenticated) {
      this.userName = '';

      return;
    }

    let user: User = this.authService.getAuthenticator();

    this.userName = user.fullName;
  }

  public getSampleProducts(){
    this.productService.getAll().subscribe(pro => {
      for (let i = 0; i < pro.length; i++){
        this.products.push(pro[i])
        if (i > 2) break;
      }
    })
  }

  public goProducts() {
    this.checkrole()
    this.router.navigate(['products']);
  }

  public checkrole() {
    if (this.authenticated == true){
      let user: User = this.authService.getAuthenticator();
      if(user.roles == 'ADMIN'){
        this.admin = true;
      }else this.admin = false;
    }
  }

}
