import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthorizationService} from '../authorization.service';
import {User} from "../../user/user";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  public authenticated: boolean = false;
  public admin: boolean = false;

  constructor(private authService: AuthorizationService, private router: Router) {
    this.authenticated = authService.hasAuthorization();

    authService.authorized$.subscribe(
      authorized => {
        this.authenticated = authorized;
        this.checkrole()
      }
    );
  }

  public goHome() {
    this.checkrole()
    this.router.navigate(['']);
  }

  public goUsers() {
    this.checkrole()
    this.router.navigate(['users']);
  }

  public goProducts() {
    this.checkrole()
    this.router.navigate(['products']);
  }
  public goCart() {
    this.checkrole()
    this.router.navigate(['cart']);
  }

  public goLogin() {
    this.checkrole()
    this.router.navigate(['login']);
  }

  public logout() {
    this.authService.deleteAuthorization();
    this.goHome();
  }

  public checkrole() {
    if (this.authenticated == true){
      let user: User = this.authService.getAuthenticator();
      if(user.roles == 'ADMIN'){
        this.admin = true;
      }else this.admin = false;
    }
  }
}
