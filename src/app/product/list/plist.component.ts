import { Component, OnInit } from '@angular/core';
import {ProductService} from "../product.service";
import { PlistDatasource} from "./plist.datasource";
import {AuthorizationService} from "../../shared/authorization.service";
import {User} from "../../user/user";
import {Product} from "../product";
import {CartService} from "../../cart/cart.service";
import {Cart} from "../../cart/cart";
import {Router} from "@angular/router";

@Component({
  selector: 'app-list',
  templateUrl: './plist.component.html',
  styleUrls: ['./plist.component.css']
})
export class PlistComponent implements OnInit {
  public displayedColumns = ['productName', 'discription', 'price', 'pictureLocation', 'btn'];
  public dataSource = null;
  public  admin = false;
  public productsList: Product[];

  constructor(private productService: ProductService,private cartService: CartService, private authService: AuthorizationService, private router: Router) {
    this.getProductList()
    let user: User = this.authService.getAuthenticator();
    if(user.roles == 'ADMIN'){
      this.admin = true;
    }else  this.admin = false;
  }

  private getProductList() {
    this.productService.getAll().subscribe(
      products => {
        this.productsList = products;
        this.dataSource = new PlistDatasource(products);
      }
    );
  }

  public onSave(product:Product) {
    this.productService.update(product);
  }

  public remove(product:Product){
    this.productService.remove(product);
    setTimeout( callback => {
      this.getProductList();
    },1000);
  }

  public addToCart(productId:string){
    let user: User = this.authService.getAuthenticator();
    let card: Cart = new Cart(null,user.emailAddress,productId);
    this.cartService.addToCart(card);
  }

  public hasData() {
    return this.dataSource !== null;
  }

  public goAdd(){
    this.router.navigate(['addProduct']);
  }

  ngOnInit() {
  }

}
