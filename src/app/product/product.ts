
export class Product
{
  constructor(
    public id?: number,
    public productName?: string,
    public discription?: string,
    public price?: string,
    public pictureLocation?: string)
  {

  }
}
