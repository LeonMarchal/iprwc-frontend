import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ApiService} from '../shared/api.service';
import {Product} from './product';
import {Router} from "@angular/router";

@Injectable()
export class ProductService {
  constructor(private api: ApiService, private router: Router) {

  }

  public getAll(): Observable<Product[]> {
    return this.api.get<Product[]>('products');
  }

  public getById(id: String): Observable<Product[]> {
    return this.api.get<Product[]>('products/'+ id);
  }


  addProduct(product: Product) {
    let data =
      {
        id: product.id,
        productName: product.productName,
        discription: product.discription,
        price: product.price,
        pictureLocation: product.pictureLocation
      };

    this.api.post<void>('products', data).subscribe
    (
      data => {
        this.goToProducts();
      },
      error => {
        alert('toevoegen is mislukt');
      }
    );
  }

  private goToProducts() {
    this.router.navigate(['products']);
  }

  update(product: Product) {
    let data =
      {
        id: product.id,
        productName: product.productName,
        discription: product.discription,
        price: product.price,
        pictureLocation: product.pictureLocation
      };

    this.api.put('products/' + product.id, product).subscribe
    (
      data => {
      },
      error => {
        alert('Het registreren is mislukt');
      }
    );
  }

  remove(product: Product){
    this.api.delete("products/" + product.id).subscribe();
  }
}
