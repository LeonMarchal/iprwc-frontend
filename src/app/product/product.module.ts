import { NgModule } from '@angular/core';
import { PublicModule } from '../public.module';
import { PlistComponent } from './list/plist.component';
import {ProductService} from "./product.service";
import { AddproductComponent } from './addproduct/addproduct.component';

@NgModule({
  imports: [PublicModule],
  declarations: [PlistComponent, AddproductComponent],
  providers: [ProductService]
})
export class ProductModule { }
